<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function welcome(){
        return view('welcome');
    }
    public function welcome_post(Request $request){
        $nama_depan= $request['fname'];
        $nama_belakang= $request['lname'];
        $nama_lengkap= $nama_depan. " ". $nama_belakang;
        $asal= $request['nationality'];
        // dd($request->all());
        return view('welcome', ['nama_lengkap'=>$nama_lengkap, 'asal' =>$asal]);
    }
}
